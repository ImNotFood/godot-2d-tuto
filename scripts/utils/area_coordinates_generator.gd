class_name AreaCoordinatesGenerator extends Node2D

var polygon: Polygon2D

func _init(_polygon: Polygon2D):
    polygon = _polygon

func get_range(axis: String) -> FloatRange:
    var min = polygon.polygon[0][axis]
    var max = polygon.polygon[0][axis]
    for vector in polygon.polygon:
        var current_value = vector[axis]
        if current_value < min:
            min = current_value
        if current_value > max:
            max = current_value
    return FloatRange.new(
        min * polygon.scale[axis] + polygon.position[axis],
        max * polygon.scale[axis] + polygon.position[axis]
    )

func get_y_ranges(x: float) -> FloatRanges:
    return RayCasting.get_y_ranges(polygon, x)
