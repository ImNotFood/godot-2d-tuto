class_name RayCasting extends Node

static func get_y_ranges(polygon: Polygon2D, x: float) -> FloatRanges:
    var points := polygon.polygon
    var y_values: Array[float] = []
    for i in range(points.size()):
        var point1 = array_at(points, i)
        var point2 = array_at(points, i+1)
        var x1 = point1.x * polygon.scale.x + polygon.position.x
        var x2 = point2.x * polygon.scale.x + polygon.position.x
        var y1 = point1.y * polygon.scale.y + polygon.position.y
        var y2 = point2.y * polygon.scale.y + polygon.position.y
        if x >= min(x1, x2) && x < max(x1, x2):
            var y = RayCasting.calculate_y_from_x_on_segment(x1, y1, x2, y2, x)
            y_values.append(y)
            if (Vector2(x, y) == point1 || Vector2(x, y) == point2):
                y_values.append(y)
    y_values.sort()
    return FloatRanges.new(y_values)

static func calculate_y_from_x_on_segment(x1: float, y1: float, x2: float, y2: float, x: float):
    return y1 + ((y2 - y1) / (x2 - x1)) * (x - x1)

static func array_at(array: Array, index: int):
    return array[index % array.size()];
