class_name RandomizeEnemyMovement extends Node2D

const speed := 2.5
var parent_node: Node2D
var vector_rotation: Vector2
var coordinate_generator: AreaCoordinatesGenerator
var screen_center: Vector2
var screen_ratio: float

func _init(spawn_area: Polygon2D):
    coordinate_generator = AreaCoordinatesGenerator.new(spawn_area)

func _ready():
    var screen_width: float = ProjectSettings.get_setting("display/window/size/viewport_width")
    var screen_height: float = ProjectSettings.get_setting("display/window/size/viewport_height")
    screen_center = Vector2(screen_width / 2, screen_height / 2)
    screen_ratio = screen_width / screen_height
    parent_node.position = get_initial_coordinates()
    parent_node.rotation = get_initial_rotation(parent_node.position)
    vector_rotation = Vector2.from_angle(parent_node.rotation)

func _process(delta):
    parent_node.position += vector_rotation * speed

func get_initial_coordinates() -> Vector2:
    var horizontal := coordinate_generator.get_range("x").random_number()
    var vertical := coordinate_generator.get_y_ranges(horizontal).random_number()
    return Vector2(horizontal, vertical)

func get_initial_rotation(position: Vector2) -> float:
    var center_angle := (screen_center - position).angle()
    var _range := FloatRange.new(
        center_angle + PI * screen_ratio / 4,
        center_angle - PI * screen_ratio / 4
    )
    return _range.random_number()
