class_name RandomizeEnemySprite extends Node2D

static var textures: Array[Array] = [
    [preload("res://graphics/enemyFlyingAlt_1.png"), preload("res://graphics/enemyFlyingAlt_2.png")],
    [preload("res://graphics/enemySwimming_1.png"), preload("res://graphics/enemySwimming_2.png")],
    [preload("res://graphics/enemyWalking_1.png"), preload("res://graphics/enemyWalking_2.png")]
]

var parent_node: Enemy
const animation_duration: float = 0.8

func _ready():
    var animation_library: AnimationLibrary = AnimationLibrary.new()
    var current_texture = textures.pick_random()
    animation_library.add_animation("movement", create_animation(current_texture))

    var animation_player: AnimationPlayer = parent_node.get_node("EnemyAnimation")
    animation_player.add_animation_library("library", animation_library)
    animation_player.play("library/movement")

func create_animation(current_texture: Array) -> Animation:
    var animation: Animation = Animation.new()
    animation.length = animation_duration
    animation.loop = true

    var track_index = animation.add_track(Animation.TYPE_VALUE)
    var sprite: Sprite2D = parent_node.get_node("EnemySprite")    
    var texture_path: String = "/" + sprite.get_path().get_concatenated_names() + ":texture"
    animation.track_set_path(track_index, texture_path)

    var size = current_texture.size()
    for i in range(size):
        animation.track_insert_key(track_index, animation_duration * i / size, current_texture[i])

    return animation
