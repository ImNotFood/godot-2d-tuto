class_name Level extends Node2D

static var enemy_resource: Resource = preload("res://scenes/enemy.tscn")

var enemies: Array[Enemy] = []
var max_enemies := 20

@onready
var spawn_timer: Timer = get_node("EnemyCooldown")

func _ready():
    spawn_timer.one_shot = true

func _process(delta: float):
    if enemies.size() < max_enemies && spawn_timer.is_stopped():
        spawn_enemy()
        spawn_timer.start()

func spawn_enemy():
    var enemy: Enemy = enemy_resource.instantiate().duplicate()
    var spawn_area = get_node("SpawnArea")
    enemy.add_script(preload("res://scripts/level1/randomize_enemy_movement.gd").new(spawn_area))
    enemy.add_script(preload("res://scripts/level1/randomize_enemy_sprite.gd").new())
    enemies.append(enemy)
    add_child(enemy)
    move_child(enemy, 0)
