class_name Enemy extends Node2D

var scripts: Array[Node2D] = []

func _ready():
    for script in scripts:
        if script.has_method("_ready"):
            script._ready()

func _process(delta: float):
    for script in scripts:
        if script.has_method("_process"):
            script._process(delta)

func add_script(node: Node):
    node.parent_node = self
    scripts.append(node)
