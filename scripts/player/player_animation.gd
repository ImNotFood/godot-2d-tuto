extends AnimationPlayer

func _ready():
    var player: Player = get_owner()
    player.movement_change.connect(change_animation)

func change_animation(direction: Vector2):
    if (direction.y != 0):
        play("player_walk_vertical")
    elif (direction.x != 0):
        play("player_walk_horizontal")
    else:
        play("RESET")
        stop()
