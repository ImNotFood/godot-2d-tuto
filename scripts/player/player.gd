class_name Player extends Node2D

signal movement_change

const speed := 400
var lastDirection := Vector2.ZERO

func _process(delta: float):
    var velocity := Vector2.ZERO
    var direction := Vector2.ZERO
    
    if Input.is_key_pressed(KEY_Z):
        velocity.y = -speed
        direction.y = -1
    elif Input.is_key_pressed(KEY_S):
        velocity.y = speed
        direction.y = 1
    if Input.is_key_pressed(KEY_Q):
        velocity.x = -speed
        direction.x = -1
    elif Input.is_key_pressed(KEY_D):
        velocity.x = speed
        direction.x = 1
        
    if direction != lastDirection:
        lastDirection = direction
        movement_change.emit(direction)

    position += velocity * delta

