extends Sprite2D

func _ready():
    var player: Player = get_owner()
    player.movement_change.connect(mirror_to_match_movement)

func mirror_to_match_movement(direction: Vector2):
    if (direction.y == 1):
        flip_v = true
    else:
        flip_v = false
    if (direction.x == -1):
        flip_h = true
    elif (direction.x == 1):
        flip_h = false
