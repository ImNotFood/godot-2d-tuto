class_name FloatRange

var from: float
var to: float

func _init(_from: float, _to: float):
    from = min(_from, _to)
    to = max(_from, _to)

func random_number() -> float:
    return randf_range(from, to)
