class_name FloatRanges extends Node

var ranges: Array[FloatRange] = []

func _init(values: Array[float]):
    for i in range(0, values.size(), 2):
        ranges.append(FloatRange.new(values[i], values[i+1]))

func append(_range: FloatRange):
    ranges.append(_range)

func random_number() -> float:
    var compressed_upper_limit = ranges.reduce(func(total, _range): return total + _range.to - _range.from, ranges[0].from)
    var result = randf_range(ranges[0].from, compressed_upper_limit)
    for i in range(ranges.size()):
        var _range = ranges[i]
        if result >= _range.from && result < _range.to:
            return result
        result += ranges[i+1].from - _range.to
    return -1 # Never accessed but the compiler cries without it
